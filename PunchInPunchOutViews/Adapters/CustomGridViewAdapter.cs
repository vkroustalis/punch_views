﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using PunchInPunchOutViews.Fragments;

namespace PunchInPunchOutViews.Adapters
{
    public class CustomGridViewAdapter : BaseAdapter
    {
        Context context;
        List<Tuple<string, int, string>> gridViewData;

        public CustomGridViewAdapter(Context c, List<Tuple<string, int, string>> gridViewData)
        {
            context = c;
            this.gridViewData = gridViewData;
        }

        public override int Count
        {
            get { return gridViewData.Count; }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return 0;
        }

        // create a new ImageView for each item referenced by the Adapter
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            Holder holder = new Holder();

            if (view != null)
            {
                
            }

            if (view == null)
            {
                LayoutInflater inflater = (LayoutInflater)context.GetSystemService(Context.LayoutInflaterService);

                view = ((Activity)context).LayoutInflater.Inflate(Resource.Layout.punchGridViewCell, null);
                holder.Badge = view.FindViewById<TextView>(Resource.Id.badgeTextView);
                holder.Image = view.FindViewById<ImageView>(Resource.Id.imageView);
                holder.Label = view.FindViewById<TextView>(Resource.Id.labelTextView);

                holder.Badge.Text = gridViewData[position].Item1;
                holder.Image.SetImageResource(gridViewData[position].Item2);
                holder.Label.Text = gridViewData[position].Item3;

                view.Tag = holder;
            }
            else
            {
                holder = (Holder)view.Tag;
            }

            return view;
        }
    }



    public class Holder : Java.Lang.Object
    {
        public TextView Badge { get; set; }
        public ImageView Image { get; set; }
        public TextView Label { get; set; }
    }
}