﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

namespace PunchInPunchOutViews.Activities
{
    [Activity(Label = "Unsuccessful Punch")]
    public class FailureActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetTheme(Resource.Style.Theme_Ess);

            SetContentView(Resource.Layout.SC1445E_PunchFailureScreen);

            var restriction = FindViewById<TextView>(Resource.Id.restrictionTextView);
            restriction.SetTextColor(Android.Graphics.Color.Red);

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.punch_success_activity_toolbar);

            //Toolbar set up
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);
        }
    }
}