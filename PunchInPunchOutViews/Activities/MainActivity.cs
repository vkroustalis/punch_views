﻿using Android.App;
using Android.Widget;
using Android.OS;
using PunchInPunchOutViews.Adapters;
using System.Collections.Generic;
using System;
using Android.Views;

using PunchInPunchOutViews.Fragments;

namespace PunchInPunchOutViews
{
    [Activity(Label = "PunchInPunchOutViews", MainLauncher = true)]
    public class MainActivity : Activity
    {
        GridView punchGridView;

        List<Tuple<string, int, string>> punchGridViewData;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            //Set Theme
            SetTheme(Android.Resource.Style.ThemeMaterialLight);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.SC1445_PunchManagment);

            //populate list with data for adapter
            punchGridViewData = new List<Tuple<string, int, string>>();
            punchGridViewData.Add(new Tuple<string, int, string>("1", Resource.Drawable.reasons_punch_neutral, "Punch"));
            punchGridViewData.Add(new Tuple<string, int, string>("1", Resource.Drawable.reasons_punch_neutral, "Punch"));
            punchGridViewData.Add(new Tuple<string, int, string>("1", Resource.Drawable.reasons_break_neutral, "Break"));
            punchGridViewData.Add(new Tuple<string, int, string>("1", Resource.Drawable.reasons_call_back_neutral, "Call Back"));
            punchGridViewData.Add(new Tuple<string, int, string>("1", Resource.Drawable.reasons_generic_neutral, "Generic"));

            punchGridView = FindViewById<GridView>(Resource.Id.punchGridView);
            punchGridView.Adapter = new CustomGridViewAdapter(this, punchGridViewData);

            punchGridView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) =>
            {
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                switch (e.Position)
                {
                    case (0):
                        ActionConfirmationDialogFragment actionDialog = new ActionConfirmationDialogFragment();
                        actionDialog.Show(transaction, "action dialog");
                        break;
                    case (1):
                        SpecialPaysDialogFragment tipDialog = new SpecialPaysDialogFragment();
                        tipDialog.Show(transaction, "tip dialog");
                        break;
                }
            };
        }
    }
}




