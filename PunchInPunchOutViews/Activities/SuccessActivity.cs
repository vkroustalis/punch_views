﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

namespace PunchInPunchOutViews.Activities
{
    [Activity(Label = "Successful Punch")]
    public class SuccessActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetTheme(Resource.Style.Theme_Ess);

            SetContentView(Resource.Layout.SC1445D_PunchSuccessScreen);

            var thankYou = FindViewById<TextView>(Resource.Id.thankYouTextView);
            thankYou.SetTextColor(Android.Graphics.Color.Black);

            var onTime = FindViewById<TextView>(Resource.Id.onTimeTextView);
            onTime.SetTextColor(Android.Graphics.Color.White);
            var rank = FindViewById<TextView>(Resource.Id.rankTextView);
            rank.SetTextColor(Android.Graphics.Color.White);

            //Toolbar set up
            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.punch_success_activity_toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayShowHomeEnabled(true);
        }
    }
}