﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using PunchInPunchOutViews.Activities;

namespace PunchInPunchOutViews.Fragments
{
    public class ActionConfirmationDialogFragment : DialogFragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            View view = inflater.Inflate(Resource.Layout.SC1445B_ActionConfirmationDialog, null);

            var yesButton = view.FindViewById<Button>(Resource.Id.yesButton);
            yesButton.Click += (object sender, EventArgs e) =>
            {
                Activity.StartActivity(typeof(SuccessActivity));
            };

            var noButton = view.FindViewById<Button>(Resource.Id.noButton);
            noButton.Click += (object sender, EventArgs e) =>
            {
                Activity.StartActivity(typeof(FailureActivity));
            };

            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
        }
    }
}